//В умові не було мови про обов'язковий запит цих двох змінних
let f0 = +prompt('Введіть число f0');
let f1 = +prompt('Введіть число f1');

let userNum = +prompt('Введіть число n')

//додав запобіжник від випадкового вводу великого числа
while ((isNaN(userNum)) || (!userNum) || (userNum > 10) || (userNum < -10)) {
   userNum = +prompt("Введіть число(!)", `${userNum}`)
}

function fib(f0, f1, n) {
   if (n === 0) {
      return f0;
   } else if (n === 1) {
      return f1;
   } else if (n > 1) {
      return fib(f0, f1, n - 1) + fib(f0, f1, n - 2);
   } else if (n < 0) {
      return fib(f0, f1, n + 2) - fib(f0, f1, n + 1);
   }
}

alert(fib(f0, f1, userNum));